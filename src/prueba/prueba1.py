##
## funcion 1

def make_fibonacci(limit, verbose=False) -> list:
    """
    Genera la serie de Fibonnaci hasta n.
    Parametro:
        limit: Hasta el numero que se debe generar
        verbose: Si debe imprimir la serie
    """
    a = 0
    b = 1
    result = []
    while a < limit:
        if verbose:
            print(a, end=', ')
        result.append(a)
        a, b = b, a+b
    if verbose:
        print()
    return result
##
## funcion 2

def muchos_items(separador, *args):
    print(separador.join(args))

##
## funcion 3

def muchos_items_palabras(**kwargs):
    for key in kwargs.keys():
        print('{0}: {1}'.format(key, kwargs[key]))

##
## funcion 4

def es_mayora_100(numero) -> bool:
    return numero > 100

##
## funcion 5

import terminal-tab
